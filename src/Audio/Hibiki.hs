{-# LANGUAGE GADTs #-}
module Audio.Hibiki where

import Data.IORef
import Control.Monad
import Control.Concurrent
import Control.Exception
import Data.Int (Int16)
import Data.Foldable
import Data.List
import SDL
import qualified Data.Vector.Storable.Mutable as V
import qualified Control.Concurrent.BoundedChan as C
import Control.Arrow
import Streamly
import qualified Streamly.Prelude as S

import qualified Data.Audio as A
import qualified Data.Array.IArray as A
import qualified Data.Array.Unboxed as A

import Codec.Wav

import Audio.Hibiki.Sound

audioCB :: C.BoundedChan Int16 -> AudioFormat sampleType -> V.IOVector sampleType -> IO ()
audioCB samples format buffer =
  case format of
    Signed16BitLEAudio ->
      do
         let n = V.length buffer
         -- print n
         traverse_ (\ptr -> V.write buffer ptr =<< C.readChan samples) [0..(n -1)]
    _ -> error "Unsupported audio format"


playMono :: MonoSample -> IO ()
playMono s = do
  initializeAll
  chan <- C.newBoundedChan $ 4096 * 16
  let
    defaultDevice = OpenDeviceSpec
      { SDL.openDeviceFreq = Mandate (fromIntegral $ samplingRate s)
      , SDL.openDeviceFormat = Mandate Signed16BitNativeAudio
      , SDL.openDeviceChannels = Mandate Mono
      , SDL.openDeviceSamples = 4096 * 2
      , SDL.openDeviceCallback = audioCB chan
      , SDL.openDeviceUsage = ForPlayback
      , SDL.openDeviceName = Nothing
      }
  bracket (openAudioDevice defaultDevice) (\(device, _) -> closeAudioDevice device)
    (\(device, audioSpec) -> do
      setAudioDevicePlaybackState device Play
      let sound = sampling s
      runStream $ S.mapM (C.writeChan chan) sound
      )


playStereo :: StereoSample -> IO ()
playStereo s = do
  initializeAll
  chan <- C.newBoundedChan $ 4096 * 16
  let
    defaultDevice = OpenDeviceSpec
      { SDL.openDeviceFreq = Mandate (fromIntegral $ samplingRate s)
      , SDL.openDeviceFormat = Mandate Signed16BitNativeAudio
      , SDL.openDeviceChannels = Mandate Stereo
      , SDL.openDeviceSamples = 4096 * 2
      , SDL.openDeviceCallback = audioCB chan
      , SDL.openDeviceUsage = ForPlayback
      , SDL.openDeviceName = Nothing
      }
  bracket (openAudioDevice defaultDevice) (\(device, _) -> closeAudioDevice device)
    (\(device, audioSpec) -> do
      setAudioDevicePlaybackState device Play
      let sound = sampling s
      runStream $ S.mapM (\(ss1, ss2) -> C.writeChan chan ss1 >> C.writeChan chan ss2) sound
      )


testSound1 = fromSound 48000 $ dropEnd 1 (Sound (\x -> sin (x * x * 440 * 6)) 12) <> takeStart 2 (freqSound cosine 440)
testSound2 = fromSound 48000 $ takeStart 10 $ Sound (unFourier $ minor 220) 30000 <> Sound (unFourier $ major 220) 4

fromAudio :: A.Audio Int16 -> Either MonoSample StereoSample
fromAudio a = case A.channelNumber a of
  1 -> Left $ SoundSample
    { sampling = S.fromFoldable $ A.elems $ A.sampleData a
    , samplingRate = A.sampleRate a
    }
  2 -> Right $ SoundSample
    { sampling = S.fromFoldable $ (\[x, y] -> (x, y)) <$> split sa
    , samplingRate = A.sampleRate a
    } where
      split [] = []
      split xs = fst (splitAt 2 xs) : split (snd $ splitAt 2 xs)
      sa = A.elems $ A.sampleData a



test1 = do
  (Right a) <- importFile "res/sound.wav"
  print a
  case fromAudio a of
    Right aa -> playStereo aa
    Left aa -> playMono aa

-- test2 f = do
--   (Right a) <- importFile f
--   print a
--   play $ fromAudio a
